---
layout: markdown_page
title: "Category Direction - Cell"
description: ""
canonical_path: "/direction/enablement/tenant-scale/cell/"
---

- TOC
{:toc}

| **Stage** | Group | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Data Stores](/direction/enablement/) | [Tenant Scale](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) | [Not Applicable](/direction/maturity/) | `2023-03-10` |

## Introduction and how you can help

Thanks for visiting this category direction page on Cells at GitLab. The Cell category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Enablement](https://about.gitlab.com/direction/enablement/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute: 
* Please comment in the linked issues and epics on this page. Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy. 
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min). Reach out to us internally in the #g_tenant-scale Slack channel.