---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the month of April.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## April 2023

### AMER Time Zone Webinars

#### DevSecOps/Compliance
##### April 25th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__ixfz3-5TH-JrA_XNE3ICQ)


### EMEA Time Zone Webinars

#### DevSecOps/Compliance
##### April 25th, 2023 at 10:00AM-11:00AM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__kkgr85eSXiw523C2NSczg)

Check back later for more webinars! 

## May 2023

### AMER Time Zone Webinars

### Intro to GitLab
#### May 2nd, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_dOE6Kz-HRDCQbPXdAOxyEw#)

### Intro to CI/CD
#### May 9th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_JdlcIhAkTkSdrKtI1EPy7Q)

### Advanced CI/CD
#### May 16th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_fFB4CRXUR1qeCPxBXLe8hQ)

#### DevSecOps/Compliance
##### May 22nd, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_2Zm7T5z1T1G6MfYRvqY7yw)

#### Getting Started with DevOps Metrics
##### May 24th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_nNREhitnSKy3OoDo2CaXdw)
