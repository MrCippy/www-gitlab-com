---
layout: handbook-page-toc
title: "Field Security Sales Training Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}


## Why a security sales training program?

One of Field Security's critical missions is to improve internal and external awareness of GitLab's security practices and the security of our platform. As we continue to iterate and grow, we must improve our ability to engage and impact prospects and customers in a meaningful way. 

The Sales organisation is a central part of this initiative. They have the critical role of selling GitLab and being trusted advisors to customers and prospects alike. From a security perspective, this role is critical from two angles:

* As the first DevSecOps platform, security is a central reason why prospects chose GitLab and want to natively integrate security into their development process
* As company supply chains are becoming more complex, security stakeholders are often central to the sales process. Showcasing the maturity of our internal security program is therefore a great competitive advantage.

The goal of the security sales enablement program is to focus on fulfilling these objectives using the approach detailed in the next sections.

## Strategy & vision

Coming in Q1FY24

### Vision

Coming in Q1FY24

### Strategy

Coming in Q1FY24

## Next generation of sales security training

Our objective is to build the next generation of GitLab's [Field Security](https://about.gitlab.com/handbook/security/security-assurance/field-security/) program. Part of enhancing this program is establishing a Security Sales Training and Enablement program. We want to spearhead the following initiatives:  

* **GitLab Security on Demand,** the Netflix of sales security enablement
* **Slack-driven enablement micro-sessions,** learn what you need, when you need it
* **AI-powered AnswerBase,** the companion to your security conversations
* **Customer-focused Security Enablement sessions**, building proficiency in the topics your prospects care about
* **Integrated feedback process,** so we can always improve the program to suit your needs

## Holistic security training and enablement 

Coming in Q2FY24

### Throughout the pipeline

Coming in Q2FY24

### During their time at GitLab

Coming in Q2FY24

### Across the different roles

Coming in Q2FY24

### In every territory

Coming in Q2FY24

### For every type of customer 

Coming in Q2FY24

## Contact us

* #sec-fieldsecurity Slack channel
* On Slack please feel free to tag @ayofan or @field-security 